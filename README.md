## General Information about IEEE CAMAD 2020 Tutorial

This repository provides the material and software for the IEEE CAMAD 2020 Tutorial "Computing in Communication Networks - From theory to practice".

First of all, thanks for your interest in our tutorial. In this repository you will find:

1. The PDFs of the presented slides.

2. The link to the repository of the Comnetsemu Virtual Machine (see below).

---
## Create your Comnetsemu Virtual Machine

It is recommended to install and use the Comnetsemu VM in order to fully participate to the tutorial.

Please follow the instructions here:

https://git.comnets.net/public-repo/comnetsemu

---
## Contacts:

E-mail: fabrizio.granelli@unitn.it